# TECNOLOGIAS USADAS

- Docker: Plataforma de contêinerização que permite empacotar, distribuir e executar aplicativos em contêineres.

- Docker Compose: Ferramenta para definir e executar aplicativos multicontêineres usando arquivos YAML.



# COMO EXECUTAR O PROJETO

## PRÉ-REQUISITOS
1. Verifique se tem o Docker instalado:
```bash
docker --version
```
Caso não esteja [clique aqui](https://docs.docker.com/get-docker/) para fazer a instalação.

2. Verifique se tem o Docker Compose instalado:
```bash
docker-compose --version
```
Caso não esteja [clique aqui](https://docs.docker.com/compose/install/) para fazer a instalação.

## INICIAR O PROJETO

1. Clone o projeto e navegue até o diretório.
2. Suba o container docker:
```bash
docker-compose up -d
```

3. Caso queria derrubar o container:
```bash
docker-compose down && docker rmi redis:latest infra-2023_app:latest
```


# COMO DESENVOLVI
Dois containers, um para a API e ou para o Redis usando uma network para que apenas a API possa ter acesso ao Redis gerando mais segurança.

O container da API possui um Dockerfile que escolher o diretório de trabalho, faz download dos pacotes, faz build do projeto, expõe a porta 3000 e executa o projeto.

O .env tem a variável PORT que indica qual porta o usuário pode ter acesso a API.



# DESAFIOS
Tive dois grandes desafios nesse projeto, entender o que é e como usar Redis e Kubernetes.

Meu problema com o Redis foi entender para que ele serve e como o projeto estava usando ele. Mas quando entendi que era apenas um banco de dados e pode ser executado dentro de um container foi bem mais fácil.

Com o Kubernetes investir muitas horas neles, mas achei bastante complexo e não conseguir desenvolver a parte do projeto que usa ele, mas estudando mais alguns acho que consigo começar a desenvolver projetos com ele.


# O QUE APRENDI
Tive a oportunidade de aprender um pouco sobre linguagem Go e entender seu objeto de ser usada em projetos grandes. Gostei bastante e espero aprender ela no futuro.

Outra coisa que aprendi foi o Redis. Pesquisando sobre ela descobrir que é bastante usado para cache de banco de dados. Estou ansioso para desenvolver alguns projetos com ele.

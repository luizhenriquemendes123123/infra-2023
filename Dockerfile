FROM golang

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /infra-2023

EXPOSE 3000

CMD ["/infra-2023"]